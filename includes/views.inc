<?php declare(strict_types=1);

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\trufil\Query\ExposedFiltersQueryAlter;

/**
 * Implements hook_query_TAG_alter().
 */
function trufil_query_trufil_has_filter_processing_alter(AlterableInterface $query): void {
  (new ExposedFiltersQueryAlter())->alter($query);
}
