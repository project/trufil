<?php declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function trufil_help($routeName, RouteMatchInterface $routeMatch) {
  switch ($routeName) {
    // Main module help for the trufil module.
    case 'help.page.trufil':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Trufil modifies the use of Views by replacing the textfield filter with widgets like number and autocomplete</em>. Views offers the ability to expose filters to the end user. When you expose a filter, you allow the user to interact with the view making it easy to build an advanced search. Trufil gives you greater control over the rendering of exposed filters.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dt>' . t('Editing or Creating Views') . '</dt>';
      $output .= '<dd>' . t('Trufil is used on <a href=":views">Views</a> that use an exposed filter. Views filters are used to reduce the result set of a View to a manageable amount of data. Trufil only operates on fields that have a limited number of options such as <a href=":node">Node</a>:Type or <a href=":taxonomy">Taxonomy</a>:TermID.',
          [
            ':views' => Url::fromRoute('help.page',
              ['name' => 'views'])->toString(),
            ':node' => Url::fromRoute('help.page',
              ['name' => 'node'])->toString(),
            ':taxonomy' => (\Drupal::moduleHandler()
              ->moduleExists('taxonomy')) ? Url::fromRoute('help.page',
              ['name' => 'taxonomy'])->toString() : '#',
          ]) . '</dd>';
      $output .= '<dt>' . t('Styling Trufil') . '</dt>';
      $output .= '<dd>' . t('Trufil provides some additional HTML structure to help you style your exposed filters.') . '</dd>';
      return $output;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function trufil_form_views_ui_config_item_form_alter(&$form, FormStateInterface $formState) {
  // Checks if Token module is enabled.
  if (!\Drupal::moduleHandler()->moduleExists('token')) {
    $text = t('Enable the Token module to allow token replacement in this field.');
    if (empty($form['options']['expose']['description']['#description'])) {
      $form['options']['expose']['description']['#description'] = $text;
    }
    else {
      $form['options']['expose']['description']['#description'] .= " $text";
    }
    return;
  }
  // Adds global token replacements, if available.
  $text = t('Tokens are allowed in this field. Replacement options can be found in the "Global replacement patterns" section, below.');
  if (empty($form['options']['expose']['description']['#description'])) {
    $form['options']['expose']['description']['#description'] = $text;
  }
  else {
    $form['options']['expose']['description']['#description'] .= " $text";
  }
  $form['options']['expose']['global_replacement_tokens'] = [
    '#title' => t('Global replacement patterns (for description field only)'),
    '#type' => 'details',
    '#weight' => 151,
  ];
  $form['options']['expose']['global_replacement_tokens']['list'] = [
    '#theme' => 'token_tree_link',
    '#token_types' => [],
  ];
}
