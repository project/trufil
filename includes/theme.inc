<?php declare(strict_types=1);

/**
 * @file
 * Theme hooks, preprocessing and suggestions.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;

/**
 * Implements hook_theme().
 */
function trufil_theme($existing, $type, $theme, $path): array {
  return [
    'trufil_links' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_alter().
 */
function trufil_theme_suggestions_alter(array &$suggestions, array $variables, $hook): void {
  // Target trufil elements.
  if ($hook === 'form_element') {
    $plugin_type = $variables['element']['#context']['#plugin_type'] ?? '';
    if ($plugin_type === 'trufil') {
      $view_id = $variables['element']['#context']['#view_id'];
      $display_id = $variables['element']['#context']['#display_id'];

      if ($view_id) {
        $suggestions[] = $hook . '__' . $view_id;
        if ($display_id) {
          $suggestions[] = $hook . '__' . $view_id . '__' . $display_id;
        }
      }
    }
  }
}

/**
 * Prepares variables for views exposed form templates.
 *
 * Default template: views-exposed-form.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 */
function trufil_preprocess_views_exposed_form(array &$variables): void {
  // Checks if Token module is enabled.
  if (!\Drupal::moduleHandler()->moduleExists('token')) {
    return;
  }

  // Replaces tokens in description field of the exposed filter.
  foreach ($variables['form']['#info'] as $name => &$info) {
    if (isset($info['description']) && isset($variables['form'][explode('filter-', $name)[1]]['#description'])) {
      $info['description'] = \Drupal::service('token')->replace($info['description']);
      $variables['form'][explode('filter-', $name)[1]]['#description'] = $info['description'];
    }
  }
}

/**
 * Prepares variables for trufil-links template.
 *
 * Default template: trufil-links.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the exposed form element.
 */
function template_preprocess_trufil_links(array &$variables): void {
  // Collect some variables before we start tweaking the element.
  $element = &$variables['elements'];
  $options = $element['#options'];
  $name = $element['#name'];
  $multiple = $element['#multiple'] ?? FALSE;

  // Set multiple attribute.
  if ($multiple) {
    $variables['attributes']['multiple'] = 'multiple';
  }

  // Set element name.
  $variables['attributes']['name'] = $name;

  // Get the query string arguments from the current request.
  $existing_query = \Drupal::service('request_stack')->getCurrentRequest()->query->all();

  // Remove page parameter from query.
  unset($existing_query['page']);

  // Store selected values.
  $selectedValues = $element['#value'];
  if (!is_array($selectedValues)) {
    $selectedValues = [$selectedValues => $selectedValues];
  }
  $variables['selected'] = $selectedValues;

  // Set hidden elements array.
  $hiddens = [];
  foreach ($options as $k => $v) {
    if (!empty($selectedValues[$k])) {
      $hidden_name = $multiple ? $name . '[' . $k . ']' : $name;
      $hiddens[$hidden_name] = $selectedValues[$k];
    }
  }
  $variables['hiddens'] = $hiddens;

  $variables['links'] = [];
  foreach ($options as $optionValue => $optionLabel) {
    // Build a new Url object for each link since the query string changes with
    // each option.
    /** @var Drupal\Core\Url $url */
    $url = clone($element['#trufil_path']);

    // Allow visitors to toggle a filter setting on and off. This is not as
    // simple as setOptions('foo', '') as that still leaves an entry which is
    // rendered rather than removing the entry from the query string altogether.
    // Calling $url->setOption() still leaves a value behind. Instead we work
    // with the entire options array and remove items from it as needed.
    $urlOptions = $url->getOptions();

    if ($multiple) {
      $newQuery = $existing_query;
      if (!isset($newQuery[$name])) {
        $newQuery[$name] = [];
      }
      if (in_array($optionValue, $selectedValues)) {
        // Allow users to toggle an option using the same link.
        $newQuery[$name] = array_filter($newQuery[$name], function ($value) use ($optionValue) {
          return $value != $optionValue;
        });
      }
      else {
        $newQuery[$name][] = $optionValue;
      }
      if (empty($newQuery[$name])) {
        unset($newQuery[$name]);
      }

      $urlOptions['query'] = $newQuery;
    }
    else {
      if (strval($optionValue) === $element['#value']) {
        // Allow toggle link functionality -- click the same link to turn an
        // option on or off.
        $newQuery = $existing_query;
        unset($newQuery[$name]);
        if (empty($newQuery)) {
          // Remove the query string completely.
          unset($urlOptions['query']);
        }
        else {
          $urlOptions['query'] = $newQuery;
        }
      }
      else {
        $urlOptions['query'] = $existing_query;
        $urlOptions['query'][$name] = $optionValue;
      }
    }

    // Add our updated options to the Url object.
    $url->setOptions($urlOptions);

    // Provide the Twig template with an array of links.
    $variables['links'][$optionValue] = [
      '#attributes' => [
        'id' => Html::getUniqueId('edit-' . implode('-', [$name, $optionValue])),
        'name' => $name . '[' . $optionValue . ']',
        'class' => [
          'trufil-link',
        ],
      ],
      '#type' => 'link',
      '#title' => $optionLabel,
      '#url' => $url,
    ];

    /*
     * @see https://stackoverflow.com/questions/13846769/php-in-array-0-value
     */
    if (in_array(strval($optionValue), $selectedValues)) {
      $variables['links'][$optionValue]['#attributes']['class'][] = 'trufil-link--selected';
    }
  }

  // Handle nested links. But first add the links as children to the element
  // for consistent processing between checkboxes/radio buttons and links.
  $variables['elements'] = array_replace($variables['elements'], $variables['links']);
  $variables['children'] = Element::children($variables['elements']);
  if (!empty($variables['elements']['#trufil_nested'])) {
    _trufil_preprocess_nested_elements($variables);
  }
}

/******************************************************************************
 * Utility functions for trufil themed elements.
 */

/**
 * Internal function to handled nested form elements.
 *
 * Adds 'is_nested' and 'depth' $variables. Requires 'children' to be set in
 * variables array before being called.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the exposed form element.
 */
function _trufil_preprocess_nested_elements(array &$variables): void {
  // Provide a hierarchical info on the element children for the template to
  // render as a nested <ul>. Views prepends '-' characters for each level of
  // depth in the vocabulary. Store that information, but remove the hyphens as
  // we don't want to display them.
  $variables['is_nested'] = TRUE;
  $variables['depth'] = [];
  foreach ($variables['children'] as $child) {
    if ($child === 'All') {
      // For non-required filters, put the any/all option at the root.
      $variables['depth'][$child] = 0;
      // And don't change the text as it defaults to "- Any -" and we do not
      // want to remove the leading hyphens.
      continue;
    }

    if (!empty($variables['elements'])) {
      $element = &$variables['elements'];
    }
    else {
      $element = &$variables['element'];
    }

    $original = $element[$child]['#title'];
    $element[$child]['#title'] = ltrim($original, '-');
    $variables['depth'][$child] = strlen($original) - strlen($element[$child]['#title']);
  }
}
