<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Provides the widget plugin manager.
 */
class TrufilWidgetManager extends DefaultPluginManager {

  /**
   * Constructs a new TrufilFilterWidgetManager object.
   *
   * @param string $type
   *   The plugin type, for example filter, pager or sort.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(protected string $type, \Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $moduleHandler) {
    $pluginInterface = 'Drupal\trufil\Plugin\TrufilWidgetInterface';
    $pluginDefinitionAnnotationName = 'Drupal\trufil\Annotation\Trufil' . Container::camelize($type) . 'Widget';
    parent::__construct("Plugin/trufil/$type", $namespaces, $moduleHandler, $pluginInterface, $pluginDefinitionAnnotationName);

    $this->alterInfo('trufil_trufil_' . $type . '_widget_info');
    $this->setCacheBackend($cacheBackend, 'trufil:' . $type . '_widget');
  }

}
