<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\views\exposed_form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Url;
use Drupal\trufil\Plugin\TrufilWidgetManager;
use Drupal\views\Plugin\views\exposed_form\InputRequired;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Exposed form plugin that provides a basic exposed form.
 *
 * @ingroup views_exposed_form_plugins
 *
 * @ViewsExposedForm(
 *   id = "trufil",
 *   title = @Translation("Trufil"),
 *   help = @Translation("Provides useful stuff for exposed form elements.")
 * )
 */
class Trufil extends InputRequired {

  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    public readonly TrufilWidgetManager $filterWidgetManager,
    public readonly TrufilWidgetManager $pagerWidgetManager,
    public readonly TrufilWidgetManager $sortWidgetManager,
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly ElementInfoManagerInterface $elementInfo,
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    // @phpstan-ignore-next-line
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('plugin.manager.trufil_filter_widget'),
      $container->get('plugin.manager.trufil_pager_widget'),
      $container->get('plugin.manager.trufil_sort_widget'),
      $container->get('module_handler'),
      $container->get('element_info'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    // General, sort, pagers, and filter.
    $trufilOptions = [
      'general' => [
        'autosubmit' => FALSE,
        'autosubmit_exclude_textfield' => FALSE,
        'autosubmit_textfield_delay' => 500,
        'autosubmit_hide' => FALSE,
        'input_required' => FALSE,
        'allow_secondary' => FALSE,
        'secondary_label' => $this->t('Advanced options'),
        'secondary_open' => FALSE,
        'reset_button_always_show' => FALSE,
      ],
      'sort' => [
        'plugin_id' => 'default',
      ],
    ];

    // Initialize options if any sort is exposed.
    // Iterate over each sort and determine if any sorts are exposed.
    $isSortExposed = FALSE;
    /** @var \Drupal\views\Plugin\views\HandlerBase $sort */
    foreach ($this->view->display_handler->getHandlers('sort') as $sort) {
      if ($sort->isExposed()) {
        $isSortExposed = TRUE;
        break;
      }
    }

    if ($isSortExposed) {
      $trufilOptions['sort']['plugin_id'] = 'default';
    }

    // Initialize options if the pager is exposed.
    $pager = $this->view->getPager();
    if ($pager && $pager->usesExposed()) {
      $trufilOptions['pager']['plugin_id'] = 'default';
    }

    // Go through each exposed filter and set default format.
    /** @var \Drupal\views\Plugin\views\HandlerBase $filter */
    foreach ($this->view->display_handler->getHandlers('filter') as $filterId => $filter) {
      if (!$filter->isExposed()) {
        continue;
      }

      $trufilOptions['filter'][$filterId]['plugin_id'] = 'default';
    }

    // Iterate over trufil options and convert them to be compatible with views
    // default options.
    $options += $this->createOptionDefaults(['trufil' => $trufilOptions]);

    return $options;
  }

  /**
   * Creates a list of view handler default options.
   *
   * Views handlers expect default options in a specific format.
   *
   * @param array $options
   *   An array of plugin defaults.
   *
   * @return array
   *   An array of plugin options.
   *
   * @see \Drupal\views\Plugin\views\PluginBase::setOptionDefaults
   */
  protected function createOptionDefaults(array $options): array {
    $result = [];
    foreach ($options as $key => $option) {
      if (is_array($option)) {
        $result[$key]['contains'] = $this->createOptionDefaults($option);
      }
      else {
        $result[$key]['default'] = $option;
      }
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $formState): void {
    // Ensure that the form values are stored in their original location, and
    // not dependent on their position in the form tree. We are moving around
    // a few elements to make the UI more user-friendly.
    $originalForm = [];
    parent::buildOptionsForm($originalForm, $formState);

    foreach (Element::children($originalForm) as $element) {
      $originalForm[$element]['#parents'] = ['exposed_form_options', $element];
    }

    // Save shorthand for trufil options.
    $trufilOptions = $this->options['trufil'];

    // User raw user input for AJAX callbacks.
    $userInput = $formState->getUserInput();
    $trufilInput = $userInput['exposed_form_options']['trufil'] ?? NULL;

    /*
     * General trufil settings
     */
    // Reorder some existing form elements.
    $form['trufil']['general']['submit_button'] = $originalForm['submit_button'];
    $form['trufil']['general']['reset_button'] = $originalForm['reset_button'];
    $form['trufil']['general']['reset_button_label'] = $originalForm['reset_button_label'];

    $form['trufil']['general']['reset_button_always_show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always show reset button'),
      '#description' => $this->t('Will keep the reset button visible even without user input.'),
      '#default_value' => $trufilOptions['general']['reset_button_always_show'],
      '#states' => [
        'invisible' => [
          'input[name="exposed_form_options[reset_button]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    // Add the 'auto-submit' functionality.
    $form['trufil']['general']['autosubmit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable auto-submit'),
      '#description' => $this->t('Automatically submits the form when an element has changed.'),
      '#default_value' => $trufilOptions['general']['autosubmit'],
    ];

    $form['trufil']['general']['autosubmit_exclude_textfield'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude Textfield'),
      '#description' => $this->t('Exclude textfields from auto-submit. User will have to press enter key, or click submit button.'),
      '#default_value' => $trufilOptions['general']['autosubmit_exclude_textfield'],
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][autosubmit]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['trufil']['general']['autosubmit_textfield_delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay for textfield autosubmit'),
      '#description' => $this->t('Configure a delay in ms before triggering autosubmit on textfields.'),
      '#default_value' => $trufilOptions['general']['autosubmit_textfield_delay'],
      '#min' => 0,
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][autosubmit]"]' => ['checked' => TRUE],
          ':input[name="exposed_form_options[trufil][general][autosubmit_exclude_textfield]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['trufil']['general']['autosubmit_hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide submit button'),
      '#description' => $this->t('Hides submit button if auto-submit and javascript are enabled.'),
      '#default_value' => $trufilOptions['general']['autosubmit_hide'],
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][autosubmit]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Insert a checkbox to make the input required optional just before the
    // input required text field. Only show the text field if the input required
    // option is selected.
    $form['trufil']['general']['input_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Input required'),
      '#description' => $this->t('Only display results after the user has selected a filter option.'),
      '#default_value' => $trufilOptions['general']['input_required'],
    ];
    $originalForm['text_input_required'] += [
      '#states' => [
        'visible' => [
          'input[name="exposed_form_options[trufil][general][input_required]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['trufil']['general']['text_input_required'] = $originalForm['text_input_required'];

    /*
     * Allow exposed form items to be displayed as secondary options.
     */
    $form['trufil']['general']['allow_secondary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable secondary exposed form options'),
      '#default_value' => $trufilOptions['general']['allow_secondary'],
      '#description' => $this->t('Allows you to specify some exposed form elements as being secondary options and places those elements in a collapsible "details" element. Use this option to place some exposed filters in an "Advanced Search" area of the form, for example.'),
    ];
    $form['trufil']['general']['secondary_label'] = [
      '#type' => 'textfield',
      '#default_value' => $trufilOptions['general']['secondary_label'],
      '#title' => $this->t('Secondary options label'),
      '#description' => $this->t(
        'The name of the details element to hold secondary options. This cannot be left blank or there will be no way to show/hide these options.'
      ),
      '#states' => [
        'required' => [
          ':input[name="exposed_form_options[trufil][general][allow_secondary]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][allow_secondary]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['trufil']['general']['secondary_open'] = [
      '#type' => 'checkbox',
      '#default_value' => $trufilOptions['general']['secondary_open'],
      '#title' => $this->t('Secondary option open by default'),
      '#description' => $this->t('Indicates whether the details element should be open by default.'),
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][allow_secondary]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    /*
     * Add options for exposed sorts.
     */
    $documentationUri = Url::fromUri('https://drupal.org/node/1701012')->toString();
    $form['trufil']['sort']['trufil_intro'] = [
      '#markup' => '<h3>' . $this->t('Exposed Sort Settings') . '</h3><p>' . $this->t('This section lets you select additional options for exposed sorts. Some options are only available in certain situations. If you do not see the options you expect, please see the <a href=":link">BEF settings documentation page</a> for more details.', [':link' => $documentationUri]) . '</p>',
    ];

    // Iterate over each sort and determine if any sorts are exposed.
    $isSortExposed = FALSE;
    /** @var \Drupal\views\Plugin\views\HandlerBase $sort */
    foreach ($this->view->display_handler->getHandlers('sort') as $sort) {
      if ($sort->isExposed()) {
        $isSortExposed = TRUE;
        break;
      }
    }

    $form['trufil']['sort']['empty'] = [
      '#type' => 'item',
      '#description' => $this->t('No sort elements have been exposed yet.'),
      '#access' => !$isSortExposed,
    ];

    if ($isSortExposed) {
      $options = [];
      foreach ($this->sortWidgetManager->getDefinitions() as $pluginId => $definition) {
        if ($definition['class']::isApplicable()) {
          $options[$pluginId] = $definition['label'];
        }
      }

      $form['trufil']['sort']['configuration'] = [
        '#prefix' => "<div id='trufil-sort-configuration'>",
        '#suffix' => "</div>",
        '#type' => 'container',
      ];

      // Get selected plugin_id on AJAX callback directly from the form state.
      $selectedPluginId = $trufilInput['sort']['configuration']['plugin_id'] ??
        $trufilOptions['sort']['plugin_id'];

      $form['trufil']['sort']['configuration']['plugin_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Display exposed sort options as'),
        '#default_value' => $selectedPluginId,
        '#options' => $options,
        '#description' => $this->t('Select a format for the exposed sort options.'),
        '#ajax' => [
          'event' => 'change',
          'effect' => 'fade',
          'progress' => 'throbber',
          // Since views options forms are complex, they're built by
          // Drupal in a different way. To bypass this problem we need to
          // provide the full path to the Ajax callback.
          'callback' => __CLASS__ . '::ajaxCallback',
          'wrapper' => 'trufil-sort-configuration',
        ],
      ];

      // Move some existing form elements.
      $form['trufil']['sort']['configuration']['exposed_sorts_label'] = $originalForm['exposed_sorts_label'];
      $form['trufil']['sort']['configuration']['expose_sort_order'] = $originalForm['expose_sort_order'];
      $form['trufil']['sort']['configuration']['sort_asc_label'] = $originalForm['sort_asc_label'];
      $form['trufil']['sort']['configuration']['sort_desc_label'] = $originalForm['sort_desc_label'];

      if ($selectedPluginId) {
        $pluginConfiguration = $trufilOptions['sort'] ?? [];
        /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
        $plugin = $this->sortWidgetManager->createInstance($selectedPluginId, $pluginConfiguration);
        $plugin->setView($this->view);

        $subform = &$form['trufil']['sort']['configuration'];
        $subformState = SubformState::createForSubform($subform, $form, $formState);
        $subform += $plugin->buildConfigurationForm($subform, $subformState);
      }
    }

    /*
     * Add options for exposed pager.
     */
    $documentationUri = Url::fromUri('https://drupal.org/node/1701012')->toString();
    $form['trufil']['pager']['trufil_intro'] = [
      '#markup' => '<h3>' . $this->t('Exposed Pager Settings') . '</h3><p>' . $this->t('This section lets you select additional options for exposed pagers. Some options are only available in certain situations. If you do not see the options you expect, please see the <a href=":link">BEF settings documentation page</a> for more details.', [':link' => $documentationUri]) . '</p>',
    ];
    $pager = $this->view->getPager();
    $isPagerExposed = $pager && $pager->usesExposed();

    $form['trufil']['pager']['empty'] = [
      '#type' => 'item',
      '#description' => $this->t('No pager elements have been exposed yet.'),
      '#access' => !$isPagerExposed,
    ];

    if ($isPagerExposed) {
      $options = [];

      foreach ($this->pagerWidgetManager->getDefinitions() as $pluginId => $definition) {
        if ($definition['class']::isApplicable()) {
          $options[$pluginId] = $definition['label'];
        }
      }

      $form['trufil']['pager']['configuration'] = [
        '#prefix' => "<div id='trufil-pager-configuration'>",
        '#suffix' => "</div>",
        '#type' => 'container',
      ];

      // Get selected plugin_id on AJAX callback directly from the form state.
      $selectedPluginId = $trufilInput['pager']['configuration']['plugin_id'] ??
        $trufilOptions['pager']['plugin_id'];

      $form['trufil']['pager']['configuration']['plugin_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Display exposed pager options as'),
        '#default_value' => $selectedPluginId,
        '#options' => $options,
        '#description' => $this->t('Select a format for the exposed pager options.'),
        '#ajax' => [
          'event' => 'change',
          'effect' => 'fade',
          'progress' => 'throbber',
          // Since views options forms are complex, they're built by
          // Drupal in a different way. To bypass this problem we need to
          // provide the full path to the Ajax callback.
          'callback' => __CLASS__ . '::ajaxCallback',
          'wrapper' => 'trufil-pager-configuration',
        ],
      ];

      if ($selectedPluginId) {
        $pluginConfiguration = $trufilOptions['pager'] ?? [];
        /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
        $plugin = $this->pagerWidgetManager->createInstance($selectedPluginId, $pluginConfiguration);
        $plugin->setView($this->view);

        $subform = &$form['trufil']['pager']['configuration'];
        $subformState = SubformState::createForSubform($subform, $form, $formState);
        $subform += $plugin->buildConfigurationForm($subform, $subformState);
      }
    }

    /*
     * Add options for exposed filters.
     */
    $documentationUri = Url::fromUri('https://drupal.org/node/1701012')->toString();
    $form['trufil']['filter']['trufil_intro'] = [
      '#markup' => '<h3>' . $this->t('Exposed Filter Settings') . '</h3><p>' . $this->t('This section lets you select additional options for exposed filters. Some options are only available in certain situations. If you do not see the options you expect, please see the <a href=":link">BEF settings documentation page</a> for more details.', [':link' => $documentationUri]) . '</p>',
    ];

    // Iterate over each filter and add trufil filter options.
    /** @var \Drupal\views\Plugin\views\HandlerBase $filter */
    foreach ($this->view->display_handler->getHandlers('filter') as $filterId => $filter) {
      if (!$filter->isExposed()) {
        continue;
      }

      $options = [];
      foreach ($this->filterWidgetManager->getDefinitions() as $pluginId => $definition) {
        if ($definition['class']::isApplicable($filter, $this->displayHandler->handlers['filter'][$filterId]->options)) {
          $options[$pluginId] = $definition['label'];
        }
      }

      // Alter the list of available widgets for this filter.
      $this->moduleHandler->alter('trufil_display_options', $options, $filter);

      // Get a descriptive label for the filter.
      $label = $this->t('Exposed filter @filter', [
        '@filter' => $filter->options['expose']['identifier'],
      ]);
      if (!empty($filter->options['expose']['label'])) {
        $label = $this->t('Exposed filter "@filter" with label "@label"', [
          '@filter' => $filter->options['expose']['identifier'],
          '@label' => $filter->options['expose']['label'],
        ]);
      }
      $form['trufil']['filter'][$filterId] = [
        '#type' => 'details',
        '#title' => $label,
        '#collapsed' => FALSE,
        '#collapsible' => TRUE,
      ];

      $form['trufil']['filter'][$filterId]['configuration'] = [
        '#prefix' => "<div id='trufil-filter-$filterId-configuration'>",
        '#suffix' => "</div>",
        '#type' => 'container',
      ];

      // Get selected plugin_id on AJAX callback directly from the form state.
      $selectedPluginId = $trufilInput['filter'][$filterId]['configuration']['plugin_id'] ?? $trufilOptions['filter'][$filterId]['plugin_id'];

      $form['trufil']['filter'][$filterId]['configuration']['plugin_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Exposed filter widget:'),
        '#default_value' => $selectedPluginId,
        '#options' => $options,
        '#ajax' => [
          'event' => 'change',
          'effect' => 'fade',
          'progress' => 'throbber',
          // Since views options forms are complex, they're built by
          // Drupal in a different way. To bypass this problem we need to
          // provide the full path to the Ajax callback.
          'callback' => __CLASS__ . '::ajaxCallback',
          'wrapper' => 'trufil-filter-' . $filterId . '-configuration',
        ],
      ];

      if ($selectedPluginId) {
        $pluginConfiguration = $trufilOptions['filter'][$filterId] ?? [];
        /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
        $plugin = $this->filterWidgetManager->createInstance($selectedPluginId, $pluginConfiguration);
        $plugin->setView($this->view);
        $plugin->setViewsHandler($filter);

        $subform = &$form['trufil']['filter'][$filterId]['configuration'];
        $subformState = SubformState::createForSubform($subform, $form, $formState);
        $subform += $plugin->buildConfigurationForm($subform, $subformState);
      }
    }
  }

  /**
   * The form ajax callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   *
   * @return array
   *   The form element to return.
   */
  public static function ajaxCallback(array $form, FormStateInterface $formState): array {
    $triggeringElement = $formState->getTriggeringElement();
    return NestedArray::getValue($form, array_slice($triggeringElement['#array_parents'], 0, -1));
  }

  /**
   * {@inheritDoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $formState): void {
    // Drupal only passes in a part of the views form, but we need the complete
    // form array for plugin subforms to work.
    $parentForm = $formState->getCompleteForm();
    // Save a shorthand to the trufil form.
    $trufilForm = &$form['trufil'];
    // Save a shorthand to the trufil options.
    $trufilFormOptions = $formState->getValue(['exposed_form_options', 'trufil']);

    parent::validateOptionsForm($form, $formState);

    // Skip plugin validation if we are switching between trufil plugins.
    $triggeringElement = $formState->getTriggeringElement();
    if ($triggeringElement['#type'] !== 'submit') {
      return;
    }

    // Shorthand for all filter handlers in this view.
    /** @var \Drupal\views\Plugin\views\HandlerBase[] $filters */
    $filters = $this->view->display_handler->handlers['filter'];

    // Iterate over all filter, sort and pager plugins.
    foreach ($trufilFormOptions as $type => $config) {
      // Validate exposed filter configuration.
      if ($type === 'filter') {
        foreach ($config as $filterId => $filterOptions) {
          $pluginId = $filterOptions['configuration']['plugin_id'] ?? NULL;
          if (!$pluginId) {
            continue;
          }
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->filterWidgetManager->createInstance($pluginId);
          $subform = &$trufilForm[$type][$filterId]['configuration'];
          $subformState = SubformState::createForSubform($subform, $parentForm, $formState);
          $plugin->setView($this->view);
          $plugin->setViewsHandler($filters[$filterId]);
          $plugin->validateConfigurationForm($subform, $subformState);
        }
      }
      // Validate exposed pager/sort configuration.
      elseif (in_array($type, ['pager', 'sort'])) {
        $pluginId = $config['configuration']['plugin_id'] ?? NULL;
        if (!$pluginId) {
          continue;
        }

        // Use the correct widget manager.
        if ($type === 'pager') {
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->pagerWidgetManager->createInstance($pluginId);
        }
        else {
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->sortWidgetManager->createInstance($pluginId);
        }

        $subform = &$trufilForm[$type]['configuration'];
        $subformState = SubformState::createForSubform($subform, $parentForm, $formState);
        $plugin->setView($this->view);
        $plugin->validateConfigurationForm($subform, $subformState);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $formState): void {
    // Drupal only passes in a part of the views form, but we need the complete
    // form array for plugin subforms to work.
    $parentForm = $formState->getCompleteForm();
    // Save a shorthand to the trufil form.
    $trufilForm = &$form['trufil'];

    // Reorder options based on config - some keys may have shifted because of
    // form alterations (@see self::buildOptionsForm()).
    $options = &$formState->getValue('exposed_form_options');
    $options = array_replace_recursive($this->options, $options);
    // Save a shorthand to the trufil options.
    $trufilOptions = &$options['trufil'];

    // Shorthand for all filter handlers in this view.
    /** @var \Drupal\views\Plugin\views\HandlerBase[] $filters */
    $filters = $this->view->display_handler->handlers['filter'];

    parent::submitOptionsForm($form, $formState);

    // Iterate over all filter, sort and pager plugins.
    foreach ($trufilOptions as $type => $config) {
      // Save exposed filter configuration.
      if ($type === 'filter') {
        foreach ($config as $filterId => $filterOptions) {
          $pluginId = $filterOptions['configuration']['plugin_id'] ?? NULL;
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          if (!$pluginId) {
            unset($trufilOptions['filter'][$filterId]);
            continue;
          }

          $plugin = $this->filterWidgetManager->createInstance($pluginId);
          $subform = &$trufilForm[$type][$filterId]['configuration'];
          $subformState = SubformState::createForSubform($subform, $parentForm, $formState);
          $plugin->setView($this->view);
          $plugin->setViewsHandler($filters[$filterId]);
          $plugin->submitConfigurationForm($subform, $subformState);

          $pluginConfiguration = $plugin->getConfiguration();
          $trufilOptions[$type][$filterId] = $pluginConfiguration;
        }
      }
      // Save exposed pager/sort configuration.
      elseif (in_array($type, ['pager', 'sort'])) {
        $pluginId = $config['configuration']['plugin_id'] ?? NULL;
        if (!$pluginId) {
          unset($trufilOptions[$type]);
          continue;
        }

        // Use the correct widget manager.
        if ($type === 'pager') {
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->pagerWidgetManager->createInstance($pluginId);
        }
        else {
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->sortWidgetManager->createInstance($pluginId);
        }

        $subform = &$trufilForm[$type]['configuration'];
        $subformState = SubformState::createForSubform($subform, $parentForm, $formState);
        $plugin->setView($this->view);
        $plugin->submitConfigurationForm($subform, $subformState);

        $pluginConfiguration = $plugin->getConfiguration();
        $trufilOptions[$type] = $pluginConfiguration;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(&$form, FormStateInterface $formState): void {
    parent::exposedFormAlter($form, $formState);

    // Mark form as Trufil form for easier alterations.
    $form['#context']['trufil'] = TRUE;

    // These styles are used on all exposed forms.
    $form['#attached']['library'][] = 'trufil/general';

    // Add the trufil-exposed-form class at the form level, so we can limit some
    // styling changes to just trufil forms.
    $form['#attributes']['class'][] = 'trufil-exposed-form';

    // Grab trufil options and allow modules/theme to modify them before
    // processing.
    $trufilOptions = $this->options['trufil'];
    $this->moduleHandler->alter('trufil_options', $trufilOptions, $this->view, $this->displayHandler);

    // Apply auto-submit values.
    if (!empty($trufilOptions['general']['autosubmit'])) {
      $form = array_merge_recursive($form, [
        '#attributes' => [
          'data-trufil-auto-submit-full-form' => '',
          'data-trufil-auto-submit' => '',
          'data-trufil-auto-submit-delay' => $trufilOptions['general']['autosubmit_textfield_delay'],
        ],
      ]);
      $form['actions']['submit']['#attributes']['data-trufil-auto-submit-click'] = '';
      $form['#attached']['library'][] = 'trufil/auto_submit';

      if (!empty($trufilOptions['general']['autosubmit_exclude_textfield'])) {
        $supportedTypes = ['entity_autocomplete', 'textfield'];
        foreach ($form as &$element) {
          if (in_array($element['#type'] ?? '', $supportedTypes)) {
            $element['#attributes']['data-trufil-auto-submit-exclude'] = 'trufil-auto-submit-exclude';
          }
        }
      }

      if (!empty($trufilOptions['general']['autosubmit_hide'])) {
        $form['actions']['submit']['#attributes']['class'][] = 'js-hide';
      }
    }

    // Some elements may be placed in a secondary details element (eg: "Advanced
    // search options"). Place this after the exposed filters and before the
    // rest of the items in the exposed form.
    $allowSecondary = $trufilOptions['general']['allow_secondary'];
    if ($allowSecondary) {
      $form['secondary'] = [
        '#attributes' => [
          'class' => ['trufil--secondary'],
        ],
        '#type' => 'details',
        '#title' => $trufilOptions['general']['secondary_label'],
        '#open' => $trufilOptions['general']['secondary_open'],
        // Disable until fields are added to this fieldset.
        '#access' => FALSE,
      ];
    }

    /*
     * Handle exposed sort elements.
     */
    if (isset($trufilOptions['sort']['plugin_id']) && !empty($form['sort_by'])) {
      $pluginId = $trufilOptions['sort']['plugin_id'];
      $pluginConfiguration = $trufilOptions['sort'];

      /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
      $plugin = $this->sortWidgetManager->createInstance($pluginId, $pluginConfiguration);
      $plugin->setView($this->view);
      $plugin->exposedFormAlter($form, $formState);
    }

    /*
     * Handle exposed pager elements.
     */
    $pager = $this->view->getPager();
    $isPagerExposed = $pager && $pager->usesExposed();
    if ($isPagerExposed && !empty($trufilOptions['pager']['plugin_id'])) {
      $pluginId = $trufilOptions['pager']['plugin_id'];
      $pluginConfiguration = $trufilOptions['pager'];

      /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
      $plugin = $this->pagerWidgetManager->createInstance($pluginId, $pluginConfiguration);
      $plugin->setView($this->view);
      $plugin->exposedFormAlter($form, $formState);
    }

    /*
     * Handle exposed filters.
     */

    // Shorthand for all filter handlers in this view.
    /** @var \Drupal\views\Plugin\views\HandlerBase[] $filters */
    $filters = $this->view->display_handler->handlers['filter'];

    // Iterate over all exposed filters.
    if (!empty($trufilOptions['filter'])) {
      foreach ($trufilOptions['filter'] as $filterId => $filterOptions) {
        // Sanity check: Ensure this filter is an exposed filter.
        if (empty($filters[$filterId]) || !$filters[$filterId]->isExposed()) {
          continue;
        }

        $pluginId = $filterOptions['plugin_id'];
        if ($pluginId) {
          /** @var \Drupal\trufil\Plugin\TrufilWidgetInterface $plugin */
          $plugin = $this->filterWidgetManager->createInstance($pluginId, $filterOptions);
          $plugin->setView($this->view);
          $plugin->setViewsHandler($filters[$filterId]);
          $plugin->exposedFormAlter($form, $formState);
        }
      }
    }

    // If our form has no visible filters, hide the submit button.
    $hasVisibleFilters = !empty(Element::getVisibleChildren($form));
    $form['actions']['submit']['#access'] = $hasVisibleFilters;

    if ($trufilOptions['general']['reset_button_always_show']) {
      $form['actions']['reset']['#access'] = TRUE;
    }

    // Never enable a reset button that has already been disabled.
    if (!isset($form['actions']['reset']['#access']) || $form['actions']['reset']['#access'] === TRUE) {
      $form['actions']['reset']['#access'] = $hasVisibleFilters;
    }

    // Ensure default process/pre_render callbacks are included when a Trufil
    // widget has added their own.
    foreach (Element::children($form) as $key) {
      $element = &$form[$key];
      $this->addDefaultElementInfo($element);
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function exposedFilterApplied(): bool {
    // If the input required option is set, check to see if a filter option has
    // been set.
    if (!empty($this->options['trufil']['general']['input_required'])) {
      return (bool) parent::exposedFilterApplied();
    }
    else {
      return TRUE;
    }
  }

  /**
   * Inserts a new form element before another element identified by $key.
   *
   * This can be useful when reordering existing form elements without weights.
   *
   * @param array $form
   *   The form array to insert the element into.
   * @param string $key
   *   The key of the form element you want to prepend the new form element.
   * @param array $element
   *   The form element to insert.
   *
   * @return array
   *   The form array containing the newly inserted element.
   */
  protected function prependFormElement(array $form, $key, array $element): array {
    $pos = array_search($key, array_keys($form)) + 1;
    return array_splice($form, 0, $pos - 1) + $element + $form;
  }

  /**
   * Adds default element callbacks.
   *
   * This is a workaround where adding process and pre-render functions are not
   * results in replacing the default ones instead of merging.
   *
   * @param array $element
   *   The render array for a single form element.
   *
   * @todo remove once the following issues are resolved.
   * @see https://www.drupal.org/project/drupal/issues/2070131
   * @see https://www.drupal.org/project/drupal/issues/2190333
   */
  protected function addDefaultElementInfo(array &$element): void {
    if (isset($element['#type']) && empty($element['#defaults_loaded']) && ($info = $this->elementInfo->getInfo($element['#type']))) {
      $element['#process'] = $element['#process'] ?? [];
      $element['#pre_render'] = $element['#pre_render'] ?? [];
      if (!empty($info['#process'])) {
        $element['#process'] = array_merge($info['#process'], $element['#process']);
      }
      if (!empty($info['#pre_render'])) {
        $element['#pre_render'] = array_merge($info['#pre_render'], $element['#pre_render']);
      }

      // Some processing needs to happen prior to the default form element
      // callbacks (e.g. sort). We use the custom '#pre_process' array for this.
      if (!empty($element['#pre_process'])) {
        $element['#process'] = array_merge($element['#pre_process'], $element['#process']);
      }

      // Workaround to add support for #group FAPI to all elements currently not
      // supported.
      // @todo remove once core issue is resolved.
      // @see https://www.drupal.org/project/drupal/issues/2190333
      if (!in_array('processGroup', array_column($element['#process'], 1))) {
        $element['#process'][] = ['\Drupal\Core\Render\Element\RenderElement', 'processGroup'];
        $element['#pre_render'][] = ['\Drupal\Core\Render\Element\RenderElement', 'preRenderGroup'];
      }
    }

    // Apply the same to any nested children.
    foreach (Element::children($element) as $key) {
      $child = &$element[$key];
      $this->addDefaultElementInfo($child);
    }
  }

}
