<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\query\Sql;

/**
 * Widget implementation for fields of list type.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_list",
 *   label = @Translation("List"),
 * )
 */
class Lists extends FilterWidgetBase {

  public function __construct(array $configuration, $pluginId, $pluginDefinition, private ?EntityTypeManagerInterface $entityTypeManager = NULL, private ?EntityTypeBundleInfoInterface $entityTypeBundleInfo = NULL) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $this->entityTypeManager ?: \Drupal::entityTypeManager();
    $this->entityTypeBundleInfo = $this->entityTypeBundleInfo ?: \Drupal::service('entity_type.bundle.info');
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;
    $entityTypeId = $filterOptions['entity_type'] ?? '';
    $fieldName = $filterOptions['entity_field'] ?? '';

    if ($entityTypeId && $fieldName && str_starts_with(self::getFieldDefinition($entityTypeId, $fieldName)?->getType(), 'list_')) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * Returns the definition of the field.
   *
   * @param string $entityTypeId
   *   Entity type the field belongs to.
   * @param string $fieldName
   *   The field name.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition|null
   *   The field definition.
   */
  private static function getFieldDefinition(string $entityTypeId, string $fieldName): ?BaseFieldDefinition {
    $fieldManager = \Drupal::service('entity_field.manager');
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entityTypeId);
    $isFound = FALSE;
    $fieldDefinition = NULL;

    // Find first entity type/bundle that has this field and check its type.
    foreach ($bundles as $bundle => $label) {
      if (!$isFound) {
        $entityDefinition = $fieldManager->getFieldDefinitions($entityTypeId, $bundle);
        $fieldDefinition = $entityDefinition[$fieldName] ?? NULL;

        if ($fieldDefinition) {
          $isFound = TRUE;
        }
      }
    }

    return $fieldDefinition;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'widget_type' => 'select',
      'is_multiple' => FALSE,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $form = parent::buildConfigurationForm($form, $formState);

    $form['widget_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Widget type'),
      '#options' => [
        'radios' => $this->t('Checkboxes/Radio Buttons'),
        'select' => $this->t('Select'),
      ],
      '#default_value' => $this->configuration['widget_type'],
      '#required' => TRUE,
    ];

    $form['is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple selections'),
      '#default_value' => $this->configuration['is_multiple'],
      '#description' => $this->t('Enable to allow users to select multiple items.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    // Set the autocomplete field.
    $fieldId = $this->getExposedFilterFieldId();

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapperId = $fieldId . '_wrapper';
    if (!isset($form[$fieldId]) && isset($form[$wrapperId])) {
      $element = &$form[$wrapperId][$fieldId];
    }
    else {
      $element = &$form[$fieldId];
    }

    parent::exposedFormAlter($form, $formState);

    // Change the field widget definition.
    $widgetType = $this->getWidgetType();
    $element['#type'] = $widgetType;
    $element['#options'] = $this->getOptions($widgetType);
    unset($element['#size']);

    if ($widgetType === 'select') {
      $element['#multiple'] = $this->configuration['is_multiple'];
    }

    $this->addQueryTags($formState->get('view')->getQuery(), $fieldId);
  }

  /**
   * Returns field option list.
   *
   * @param string $widgetType
   *   The form field widget type.
   *
   * @return string[]
   *   The key => label allowed values list from field settings.
   */
  private function getOptions(string $widgetType): array {
    $entityTypeId = $this->handler->options['entity_type'] ?? '';
    $fieldName = $this->handler->options['entity_field'] ?? '';

    $fieldDefinition = self::getFieldDefinition($entityTypeId, $fieldName);
    $fieldSettings = $fieldDefinition->getSettings();
    $options = $fieldSettings['allowed_values'] ?? [];

    // Add 'any' option to the list.
    if (!$this->handler->options['expose']['required'] && $widgetType !== 'checkboxes') {
      $options = ['' => $this->t('- Any -')] + $options;
    }

    return $options;
  }

  /**
   * Returns form field widget type by configuration widget type.
   *
   * @return string
   *   The form field widget type.
   */
  private function getWidgetType(): string {
    $widgetType = $this->configuration['widget_type'];

    if ($widgetType === 'radios' && $this->configuration['is_multiple']) {
      $widgetType = 'checkboxes';
    }

    return $widgetType;
  }

  /**
   * Adds tags to the query necessary for hook_query_alter().
   *
   * @param \Drupal\views\Plugin\views\query\Sql $query
   *   The view query.
   * @param string $fieldId
   *   The field name.
   *
   * @see \Drupal\trufil\Query\ExposedFiltersQueryAlter
   */
  private function addQueryTags(Sql $query, string $fieldId): void {
    $query->addTag('trufil_has_filter_processing');
    // Pass field data to alter the query on the next step.
    $query->addTag("trufil__field_id__{$fieldId}");
    $query->addTag("trufil__{$fieldId}__type__date");
    $query->addTag("trufil__{$fieldId}__table__" . $this->handler->options['table']);
    $query->addTag("trufil__{$fieldId}__field_name__" . $this->handler->options['field']);
  }

}
