<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Basic number widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_number",
 *   label = @Translation("Number"),
 * )
 */
class Number extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;

    if (is_a($filter, 'Drupal\views\Plugin\views\filter\NumericFilter')) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'min' => NULL,
      'max' => NULL,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $form = parent::buildConfigurationForm($form, $formState);

    unset($form['advanced']['placeholder_text']);
    $form['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum'),
      '#default_value' => $this->configuration['min'],
      '#description' => $this->t('Adds a min attribute to the input field.'),
    ];

    $form['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum'),
      '#default_value' => $this->configuration['max'],
      '#description' => $this->t('Adds a max attribute to the input field.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    // Set the number field.
    $fieldId = $this->getExposedFilterFieldId();

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapperId = $fieldId . '_wrapper';
    if (!isset($form[$fieldId]) && isset($form[$wrapperId])) {
      $element = &$form[$wrapperId][$fieldId];
    }
    else {
      $element = &$form[$fieldId];
    }

    parent::exposedFormAlter($form, $formState);

    // Double Number-API-based input elements such as "in-between".
    $isBetween = !empty($element['min']) && !empty($element['max']);

    if ($isBetween) {
      $element['max']['#type'] = 'number';
      $element['min']['#type'] = 'number';
      $element['max']['#attributes']['class'][] = 'trufil-number';
      $element['min']['#attributes']['class'][] = 'trufil-number';

      $max = $this->configuration['max'];
      if ($max || $max === '0') {
        $element['max']['#attributes']['max'] = $max;
      }

      $min = $this->configuration['min'];
      if ($min || $min === '0') {
        $element['min']['#attributes']['min'] = $min;
      }
    }
    else {
      $element['#type'] = 'number';
      $element['#attributes']['class'][] = 'trufil-number';

      $max = $this->configuration['max'];
      if ($max || $max === '0') {
        $element['#attributes']['max'] = $max;
      }

      $min = $this->configuration['min'];
      if ($min || $min === '0') {
        $element['#attributes']['min'] = $min;
      }
    }
  }

}
