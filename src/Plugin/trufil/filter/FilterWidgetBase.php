<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\trufil\Helper\TrufilHelper;
use Drupal\trufil\Plugin\TrufilWidgetBase;
use Drupal\trufil\Plugin\TrufilWidgetInterface;
use Drupal\views\Plugin\views\filter\NumericFilter;
use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Base class for widget plugins.
 */
abstract class FilterWidgetBase extends TrufilWidgetBase implements TrufilWidgetInterface {

  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;

    // Sanity check to ensure we have a filter to work with.
    if (is_null($filter)) {
      return FALSE;
    }

    // Check various filter types and determine what options are available.
    if (is_a($filter, 'Drupal\views\Plugin\views\filter\StringFilter') || is_a($filter, 'Drupal\views\Plugin\views\filter\InOperator')) {
      if (in_array($filter->operator, ['in', 'or', 'and', 'not'])) {
        $isApplicable = TRUE;
      }
      if (in_array($filter->operator, ['empty', 'not empty'])) {
        $isApplicable = TRUE;
      }
    }

    if (is_a($filter, 'Drupal\views\Plugin\views\filter\BooleanOperator')) {
      $isApplicable = TRUE;
    }

    if (is_a($filter, 'Drupal\taxonomy\Plugin\views\filter\TaxonomyIndexTid')) {
      // Autocomplete and dropdown taxonomy filter are both instances of
      // TaxonomyIndexTid, but we can't show Trufil options for the autocomplete
      // widget.
      if ($filterOptions['type'] === 'select') {
        $isApplicable = TRUE;
      }
    }

    if ($filter->isAGroup()) {
      $isApplicable = TRUE;
    }

    if (is_a($filter, 'Drupal\search_api\Plugin\views\filter\SearchApiFulltext')) {
      $isApplicable = TRUE;
    }

    if (is_a($filter, 'Drupal\facets_exposed_filters\Plugin\views\filter\FacetsFilter')) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'advanced' => [
        'collapsible' => FALSE,
        'is_secondary' => FALSE,
        'placeholder_text' => '',
        'rewrite' => [
          'filter_rewrite_values' => '',
        ],
        'sort_options' => FALSE,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $filterWidgetType = $this->getExposedFilterWidgetType();

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced filter options'),
      '#weight' => 10,
    ];

    // Allow users to sort options.
    $supported_types = ['select'];
    if (in_array($filterWidgetType, $supported_types)) {
      $form['advanced']['sort_options'] = [
        '#type' => 'checkbox',
        '#title' => 'Sort filter options',
        '#default_value' => !empty($this->configuration['advanced']['sort_options']),
        '#description' => $this->t('The options will be sorted alphabetically.'),
      ];
    }

    // Allow users to specify placeholder text.
    $supported_types = ['entity_autocomplete', 'textfield'];
    if (in_array($filterWidgetType, $supported_types)) {
      $form['advanced']['placeholder_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder text'),
        '#description' => $this->t('Text to be shown in the text field until it is edited. Leave blank for no placeholder to be set.'),
        '#default_value' => $this->configuration['advanced']['placeholder_text'],
      ];
    }

    // Allow rewriting of filter options for any filter. String and numeric
    // filters allow unlimited filter options via textfields, so we can't
    // offer rewriting for those.
    // @todo check other core filter types
    if ((!$filter instanceof StringFilter && !$filter instanceof NumericFilter) || $filter->isAGroup()) {
      $form['advanced']['rewrite']['filter_rewrite_values'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Rewrite the text displayed'),
        '#default_value' => $this->configuration['advanced']['rewrite']['filter_rewrite_values'],
        '#description' => $this->t('Use this field to rewrite the filter options displayed. Use the format of current_text|replacement_text, one replacement per line. For example: <pre>
  Current|Replacement
  On|Yes
  Off|No
  </pre> Leave the replacement text blank to remove an option altogether. If using hierarchical taxonomy filters, do not including leading hyphens in the current text.
          '),
      ];
    }

    // Allow any filter to be collapsible.
    $form['advanced']['collapsible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make filter options collapsible'),
      '#default_value' => !empty($this->configuration['advanced']['collapsible']),
      '#description' => $this->t(
        'Puts the filter options in a collapsible details element.'
      ),
    ];

    // Allow any filter to be moved into the secondary options' element.
    $form['advanced']['is_secondary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This is a secondary option'),
      '#default_value' => !empty($this->configuration['advanced']['is_secondary']),
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][allow_secondary]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Places this element in the secondary options portion of the exposed form.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $filterId = $filter->options['expose']['identifier'];
    $fieldId = $this->getExposedFilterFieldId();
    $isCollapsible = $this->configuration['advanced']['collapsible'];
    $isSecondary = !empty($form['secondary']) && $this->configuration['advanced']['is_secondary'];

    // Sort options alphabetically.
    if ($this->configuration['advanced']['sort_options']) {
      $form[$fieldId]['#nested'] = $filter->options['hierarchy'] ?? FALSE;
      $form[$fieldId]['#nested_delimiter'] = '-';
      $form[$fieldId]['#pre_process'][] = [$this, 'processSortedOptions'];
    }

    // Check for placeholder text.
    if (!empty($this->configuration['advanced']['placeholder_text'])) {
      // @todo Add token replacement for placeholder text.
      $form[$fieldId]['#placeholder'] = $this->configuration['advanced']['placeholder_text'];
    }

    // Handle filter value rewrites.
    if (!empty($form[$fieldId]['#options']) && $this->configuration['advanced']['rewrite']['filter_rewrite_values']) {
      // Reorder options based on rewrite values, if sort options is disabled.
      $form[$fieldId]['#options'] = TrufilHelper::rewriteOptions($form[$fieldId]['#options'], $this->configuration['advanced']['rewrite']['filter_rewrite_values'], !$this->configuration['advanced']['sort_options']);
      // @todo what is $selected?
      // if (isset($selected) &&
      // !isset($form[$fieldId]['#options'][$selected])) {
      // Avoid "Illegal choice" errors.
      // $form[$fieldId]['#default_value'] = NULL;
      // }
    }

    // Identify all exposed filter elements.
    $identifier = $filterId;
    $exposedLabel = $filter->options['expose']['label'];
    $exposedDescription = $filter->options['expose']['description'];

    if ($filter->isAGroup()) {
      $identifier = $filter->options['group_info']['identifier'];
      $exposedLabel = $filter->options['group_info']['label'];
      $exposedDescription = $filter->options['group_info']['description'];
    }

    // If selected, collect our collapsible filter form element and put it in
    // a details element.
    if ($isCollapsible) {
      $details = [];
      $details[$fieldId . '_collapsible'] = [
        '#type' => 'details',
        '#title' => $exposedLabel,
        '#description' => $exposedDescription,
        '#attributes' => [
          'class' => ['form-item'],
        ],
      ];

      if ($isSecondary) {
        // Move secondary elements.
        $this->addElementToGroup($form, $formState, $fieldId . '_collapsible', 'secondary');
      }
      // Retain same weight as the original fields for details.
      $pos = array_search($fieldId, array_keys($form));
      $form = array_merge(array_slice($form, 0, $pos), $details, array_slice($form, $pos));
    }

    // Add possible field wrapper to validate for "between" operator.
    $elementWrapper = $fieldId . '_wrapper';

    $filterElements = [
      $identifier,
      $elementWrapper,
      $filter->options['expose']['operator_id'],
    ];

    // Iterate over all exposed filter elements.
    foreach ($filterElements as $element) {
      // Sanity check to make sure the element exists.
      if (empty($form[$element])) {
        continue;
      }

      // "Between" operator fields to validate for.
      $fields = ['min', 'max'];

      // Check if the element is a part of a wrapper.
      $wrapperArray = $form[$element];
      if ($element === $elementWrapper) {
        // Determine if wrapper element has min or max fields or if
        // collapsible, if so then update type.
        if (array_intersect($fields, array_keys($wrapperArray[$fieldId])) || $isCollapsible) {
          $form[$element] = [
            '#type' => 'container',
            $element => $wrapperArray,
          ];
        }
      }
      else {
        // Determine if element has min or max child fields,
        // if so then update type.
        if (array_intersect($fields, array_keys($form[$fieldId]))) {
          $form[$element] = [
            '#type' => 'container',
            $element => $wrapperArray,
          ];
        }
      }

      // Move collapsible elements.
      if ($isCollapsible) {
        $this->addElementToGroup($form, $formState, $element, $fieldId . '_collapsible');
      }
      else {
        $form[$element]['#title'] = $exposedLabel;
        $form[$element]['#description'] = $exposedDescription;

        // Move secondary elements.
        if ($isSecondary) {
          $this->addElementToGroup($form, $formState, $element, 'secondary');
        }
      }

      // Finally, add some metadata to the form element.
      $this->addContext($form[$element]);
    }
  }

  /**
   * Sorts the options for a given form element alphabetically.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return array
   *   The altered element.
   */
  public function processSortedOptions(array $element, FormStateInterface $formState): array {
    $options = &$element['#options'];

    // Ensure "- Any -" value does not get sorted.
    $anyOption = FALSE;
    if (empty($element['#required'])) {
      // We use array_slice to preserve they keys needed to determine the value
      // when using a filter (e.g. taxonomy terms).
      $anyOption = array_slice($options, 0, 1, TRUE);
      // Array_slice does not modify the existing array, we need to remove the
      // option manually.
      unset($options[key($anyOption)]);
    }

    // Not all option arrays will have simple data types. We perform a custom
    // sort in case users want to sort more complex fields (e.g taxonomy terms).
    if (!empty($element['#nested'])) {
      $delimiter = $element['#nested_delimiter'] ?? '-';
      $options = TrufilHelper::sortNestedOptions($options, $delimiter);
    }
    else {
      $options = TrufilHelper::sortOptions($options);
    }

    // Restore the "- Any -" value at the first position.
    if ($anyOption) {
      $options = $anyOption + $options;
    }

    return $element;
  }

  /**
   * Helper function to get the unique identifier for the exposed filter.
   *
   * Takes into account grouped filters with custom identifiers.
   */
  protected function getExposedFilterFieldId(): string {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $fieldId = $filter->options['expose']['identifier'] ?? '';
    $isGroupedFilter = $filter->options['is_grouped'] ?? FALSE;

    // Grouped filters store their identifier elsewhere.
    if ($isGroupedFilter) {
      $fieldId = $filter->options['group_info']['identifier'] ?? '';
    }

    return $fieldId;
  }

  /**
   * Helper function to get the widget type of the exposed filter.
   *
   * @return string
   *   The type of the form render element use for the exposed filter.
   */
  protected function getExposedFilterWidgetType(): string {
    // We need to dig into the exposed form configuration to retrieve the
    // form type of the filter.
    $form = [];
    $formState = new FormState();
    $formState->set('exposed', TRUE);
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $filter->buildExposedForm($form, $formState);
    $filterId = $filter->options['expose']['identifier'];

    return $form[$filterId]['#type'] ?? $form[$filterId]['value']['#type'] ?? '';
  }

}
