<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\Plugin\views\ViewsHandlerInterface;

/**
 * Autocomplete widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_autocomplete",
 *   label = @Translation("Autocomplete"),
 * )
 */
class Autocomplete extends FilterWidgetBase {

  public function __construct(array $configuration, $pluginId, $pluginDefinition, private ?EntityTypeManagerInterface $entityTypeManager = NULL, private ?EntityTypeBundleInfoInterface $entityTypeBundleInfo = NULL) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $this->entityTypeManager ?: \Drupal::entityTypeManager();
    $this->entityTypeBundleInfo = $this->entityTypeBundleInfo ?: \Drupal::service('entity_type.bundle.info');
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;
    $fieldData = self::getEntityReferenceFieldData($filter);

    if ($fieldData && self::getFieldDefinition($fieldData['entity_type_id'], $fieldData['field_name'])?->getType() === 'entity_reference') {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * Returns the data of the field that looks like entity reference field.
   *
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $filter
   *   A views filter definition.
   *
   * @return string[]
   *   The entity reference field data or empty array for fields of other types.
   */
  private static function getEntityReferenceFieldData(ViewsHandlerInterface $filter): array {
    $data = [];

    if (!empty($filter->options['entity_type'])) {
      $data['entity_type_id'] = $filter->options['entity_type'];
      $data['field_name'] = $filter->options['entity_field'] ?? '';
    }
    elseif (!empty($filter->configuration['entity_type'])) {
      $data['entity_type_id'] = $filter->configuration['entity_type'];
      $data['field_name'] = $filter->configuration['field_name'] ?? '';
    }

    return $data;
  }

  /**
   * Returns the definition of the field.
   *
   * @param string $entityTypeId
   *   Entity type the field belongs to.
   * @param string $fieldName
   *   The field name.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition|\Drupal\field\Entity\FieldConfig|null
   *   The field definition.
   */
  private static function getFieldDefinition(string $entityTypeId, string $fieldName) {
    $fieldManager = \Drupal::service('entity_field.manager');
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entityTypeId);
    $isFound = FALSE;
    $fieldDefinition = NULL;

    // Find first entity type/bundle that has this field and check its type.
    foreach ($bundles as $bundle => $label) {
      if (!$isFound) {
        $entityDefinition = $fieldManager->getFieldDefinitions($entityTypeId, $bundle);
        $fieldDefinition = $entityDefinition[$fieldName] ?? NULL;

        if ($fieldDefinition) {
          $isFound = TRUE;
        }
      }
    }

    return $fieldDefinition;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'handler' => '',
      'target_type' => '',
      'target_bundles' => [],
      'entity_bundle_key' => '',
      'entity_bundle_count' => 0,
      'widget_type' => 'textfield',
      'is_multiple' => FALSE,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $form = parent::buildConfigurationForm($form, $formState);

    // Get filter related field, its settings and target bundles.
    $fieldData = self::getEntityReferenceFieldData($this->handler);
    $fieldDefinition = $this::getFieldDefinition($fieldData['entity_type_id'], $fieldData['field_name']);
    $fieldSettings = $fieldDefinition?->getSettings();
    $bundles = ($fieldSettings ? $this->entityTypeBundleInfo->getBundleInfo($fieldSettings['target_type']) : []) ?: [];
    $bundles = $bundles && is_array(reset($bundles))
      ? array_combine(array_keys($bundles), array_column($bundles, 'label'))
      : $bundles;
    $entityDefinition = $this->entityTypeManager->getDefinition($fieldData['entity_type_id']);

    // If specific bundle list is configured in the field settings
    // then we should limit our options to that list.
    if (!empty($fieldSettings['handler_settings']['target_bundles'])) {
      foreach ($bundles as $bundle => $label) {
        if (empty($fieldSettings['handler_settings']['target_bundles'][$bundle])) {
          unset($bundles[$bundle]);
        }
      }
    }

    $bundleCount = count($bundles);

    if (!$bundleCount) {
      return $form;
    }

    unset($form['advanced']['placeholder_text']);

    $form['handler'] = [
      '#type' => 'value',
      '#value' => $fieldSettings['handler'],
    ];

    $form['target_type'] = [
      '#type' => 'value',
      '#value' => $fieldSettings['target_type'],
    ];

    $form['entity_bundle_key'] = [
      '#type' => 'value',
      '#value' => $entityDefinition->getKey('bundle'),
    ];

    $form['entity_bundle_count'] = [
      '#type' => 'value',
      '#value' => $bundleCount,
    ];

    if ($bundleCount > 1) {
      $form['target_bundles'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('The target bundle'),
        '#description' => $this->t('If none are selected, all are allowed.'),
        '#options' => $bundles,
        '#default_value' => $this->configuration['target_bundles'],
        '#trufil_filter_checkboxes' => TRUE,
      ];
    }

    $form['widget_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Widget type'),
      '#options' => [
        'textfield' => $this->t('Textfield'),
        'radios' => $this->t('Checkboxes/Radio Buttons'),
        'select' => $this->t('Select'),
      ],
      '#default_value' => $this->configuration['widget_type'],
      '#required' => TRUE,
    ];

    $form['is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple selections'),
      '#default_value' => $this->configuration['is_multiple'],
      '#description' => $this->t('Enable to allow users to select multiple items.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    // Set the autocomplete field.
    $fieldId = $this->getExposedFilterFieldId();

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapperId = $fieldId . '_wrapper';
    if (!isset($form[$fieldId]) && isset($form[$wrapperId])) {
      $element = &$form[$wrapperId][$fieldId];
    }
    else {
      $element = &$form[$fieldId];
    }

    parent::exposedFormAlter($form, $formState);

    $widgetType = $this->getWidgetType();
    $element['#type'] = $widgetType;

    // Change the field widget definition.
    switch ($widgetType) {
      case 'checkboxes':
      case 'radios':
        $element['#options'] = $this->getAutocompleteOptions();
        unset($element['#size']);
        break;

      case 'select':
        $element['#options'] = $this->getAutocompleteOptions();
        $element['#multiple'] = $this->configuration['is_multiple'];
        unset($element['#size']);
        break;

      default:
        $element['#handler'] = $this->configuration['handler'];
        $element['#target_type'] = $this->configuration['target_type'];
        $element['#handler_settings'] = [
          'target_bundles' => $this->configuration['target_bundles'],
        ];
        $element['#tags'] = $this->configuration['is_multiple'];
        $element['#maxlength'] = 1024;
        $element['#attributes']['class'][] = 'trufil-autocomplete';
    }

    $this->addQueryTags($formState->get('view')->getQuery(), $fieldId);
  }

  /**
   * Loads entities and returns them as options list.
   *
   * @return string[]
   *   The sorted id => label entities list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Invalid plugin definition exception.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Plugin not found exception.
   */
  private function getAutocompleteOptions(): array {
    $properties = [];
    if ($this->configuration['entity_bundle_count'] > 1 && $this->configuration['target_bundles']) {
      $properties[$this->configuration['entity_bundle_key']] = $this->configuration['target_bundles'];
    }

    // Load entities.
    $entities = $this->entityTypeManager
      ->getStorage($this->configuration['target_type'])
      ->loadByProperties($properties);

    // Format entities as id => label array.
    array_walk($entities, function (&$item) {
      $item = $item->label();
    });
    asort($entities);

    // Add 'any' option to the list.
    if (!$this->handler->options['expose']['required']) {
      $entities = ['' => $this->t('- Any -')] + $entities;
    }

    return $entities;
  }

  /**
   * Returns form field widget type by configuration widget type.
   *
   * @return string
   *   The form field widget type.
   */
  private function getWidgetType(): string {
    $widgetType = $this->configuration['widget_type'];

    if ($widgetType === 'textfield') {
      $widgetType = 'entity_autocomplete';
    }
    elseif ($widgetType === 'radios' && $this->configuration['is_multiple']) {
      $widgetType = 'checkboxes';
    }

    return $widgetType;
  }

  /**
   * Adds tags to the query necessary for hook_query_alter().
   *
   * @param \Drupal\views\Plugin\views\query\Sql $query
   *   The view query.
   * @param string $fieldId
   *   The field name.
   *
   * @see \Drupal\trufil\Query\ExposedFiltersQueryAlter
   */
  private function addQueryTags(Sql $query, string $fieldId): void {
    $query->addTag('trufil_has_filter_processing');
    // Pass field data to alter the query on the next step.
    $query->addTag("trufil__field_id__{$fieldId}");
    $query->addTag("trufil__{$fieldId}__type__autocomplete");
    $query->addTag("trufil__{$fieldId}__table__" . $this->handler->options['table']);
    $query->addTag("trufil__{$fieldId}__field_name__" . $this->handler->options['field']);
  }

}
