<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Single on/off widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_single",
 *   label = @Translation("Single On/Off Checkbox"),
 * )
 */
class Single extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;

    // Sanity check to ensure we have a filter to work with.
    if (is_null($filter)) {
      return FALSE;
    }

    if (is_a($filter, 'Drupal\views\Plugin\views\filter\BooleanOperator') || ($filter->isAGroup() && count($filter->options['group_info']['group_items']) == 1)) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    // Form element is designated by the element ID which is user-
    // configurable, and stored differently for grouped filters.
    $exposedId = $filter->options['expose']['identifier'];
    $fieldId = $this->getExposedFilterFieldId();

    parent::exposedFormAlter($form, $formState);

    if (!empty($form[$fieldId])) {
      // Views populates missing values in $formState['input'] with the
      // defaults and a checkbox does not appear in $_GET (or $_POST) so it
      // will appear to be missing when a user submits a form. Because of
      // this, instead of unchecking the checkbox value will revert to the
      // default. More, the default value for select values (i.e. 'Any') is
      // reused which results in the checkbox always checked.
      $input = $formState->getUserInput();
      // The input value ID is not always consistent.
      // Prioritize the field ID, but default to exposed ID.
      // @todo Remove $exposedId once
      //   https://www.drupal.org/project/drupal/issues/288429 is fixed.
      $input_value = $input[$fieldId] ?? ($input[$exposedId] ?? NULL);
      $checked = FALSE;
      // We need to be super careful when working with raw input values. Let's
      // make sure the value exists in our list of possible options.
      if (in_array($input_value, array_keys($form[$fieldId]['#options'])) && $input_value !== 'All') {
        $checked = (bool) $input_value;
      }
      $form[$fieldId]['#type'] = 'checkbox';
      $form[$fieldId]['#default_value'] = 0;
      $form[$fieldId]['#return_value'] = 1;
      $form[$fieldId]['#value'] = $checked ? 1 : 0;
    }
  }

}
