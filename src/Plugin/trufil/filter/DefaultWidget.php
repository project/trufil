<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

/**
 * Default widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "default",
 *   label = @Translation("Default"),
 * )
 */
class DefaultWidget extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    return TRUE;
  }

}
