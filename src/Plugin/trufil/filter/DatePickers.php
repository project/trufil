<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Date picker widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_datepicker",
 *   label = @Translation("Date Picker"),
 * )
 */
class DatePickers extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $isApplicable = FALSE;

    if ((is_a($filter, 'Drupal\views\Plugin\views\filter\Date') || is_a($filter, 'Drupal\views\Plugin\views\filter\StringFilter') || !empty($filter->date_handler)) && !$filter->isAGroup()) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    $fieldId = $this->getExposedFilterFieldId();

    // Handle wrapper element added to exposed filters
    // in https://www.drupal.org/project/drupal/issues/2625136.
    $wrapperId = $fieldId . '_wrapper';
    if (!isset($form[$fieldId]) && isset($form[$wrapperId])) {
      $element = &$form[$wrapperId][$fieldId];
    }
    else {
      $element = &$form[$fieldId];
    }

    parent::exposedFormAlter($form, $formState);

    // Double Date-API-based input elements such as "in-between".
    $isBetween = !empty($element['min']) && !empty($element['max']);

    if ($isBetween) {
      $element['min']['#type'] = 'date';
      $element['max']['#type'] = 'date';
      $element['min']['#attributes']['class'][] = 'trufil-datepicker';
      $element['max']['#attributes']['class'][] = 'trufil-datepicker';
      $element['min']['#attributes']['autocomplete'] = 'off';
      $element['max']['#attributes']['autocomplete'] = 'off';
    }
    else {
      $element['#type'] = 'date';
      $element['value']['#attributes']['class'][] = 'trufil-datepicker';
      $element['value']['#attributes']['autocomplete'] = 'off';
    }
  }

}
