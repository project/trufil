<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\trufil\Helper\TrufilHelper;

/**
 * Default widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_links",
 *   label = @Translation("Links"),
 * )
 */
class Links extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'select_all_none' => FALSE,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;

    $form = parent::buildConfigurationForm($form, $formState);

    $form['select_all_none'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add select all/none links'),
      '#default_value' => !empty($this->configuration['select_all_none']),
      '#disabled' => !$filter->options['expose']['multiple'],
      '#description' => $this->t('Add a "Select All/None" link when rendering the exposed filter using checkboxes. If this option is disabled, edit the filter and check the "Allow multiple selections".'
      ),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $filter */
    $filter = $this->handler;
    $fieldId = $this->getExposedFilterFieldId();

    parent::exposedFormAlter($form, $formState);

    if (!empty($form[$fieldId])) {
      // Clean up filters that pass objects as options instead of strings.
      if (!empty($form[$fieldId]['#options'])) {
        $form[$fieldId]['#options'] = TrufilHelper::flattenOptions($form[$fieldId]['#options']);
      }

      // Support rendering hierarchical links (e.g. taxonomy terms).
      if (!empty($filter->options['hierarchy'])) {
        $form[$fieldId]['#trufil_nested'] = TRUE;
      }

      $form[$fieldId]['#theme'] = 'trufil_links';
      // Exposed form displayed as blocks can appear on pages other than
      // the view results appear on. This can cause problems with
      // select_as_links options as they will use the wrong path. We
      // provide a hint for theme functions to correct this.
      $form[$fieldId]['#trufil_path'] = $this->getExposedFormActionUrl($formState);

      if ($filter->view->ajaxEnabled() || $filter->view->display_handler->ajaxEnabled()) {
        $form[$fieldId]['#attributes']['class'][] = 'trufil-links-use-ajax';
        $form['#attached']['library'][] = 'trufil/links_use_ajax';
      }
    }
  }

}
