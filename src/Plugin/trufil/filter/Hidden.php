<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\filter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Hidden widget implementation.
 *
 * @TrufilFilterWidget(
 *   id = "trufil_hidden",
 *   label = @Translation("Hidden"),
 * )
 */
class Hidden extends FilterWidgetBase {

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($filter = NULL, array $filterOptions = []): bool {
    $isApplicable = parent::isApplicable($filter, $filterOptions);

    if ((is_a($filter, 'Drupal\views\Plugin\views\filter\Date') || !empty($filter->date_handler)) && !$filter->isAGroup()) {
      $isApplicable = TRUE;
    }

    return $isApplicable;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    $fieldId = $this->getExposedFilterFieldId();

    parent::exposedFormAlter($form, $formState);

    if (empty($form[$fieldId]['#multiple'])) {
      // Single entry filters can simply be changed to a different element
      // type.
      $form[$fieldId]['#type'] = 'hidden';
    }
    else {
      // Hide the label.
      $form['#info']["filter-$fieldId"]['label'] = '';
      $form[$fieldId]['#title'] = '';

      // Use Trufil's preprocess and template to output the hidden elements.
      $form[$fieldId]['#theme'] = 'trufil_hidden';
    }
  }

}
