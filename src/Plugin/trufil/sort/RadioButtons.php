<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\sort;

use Drupal\Core\Form\FormStateInterface;

/**
 * Radio Buttons sort widget implementation.
 *
 * @TrufilSortWidget(
 *   id = "trufil_radios",
 *   label = @Translation("Radio Buttons"),
 * )
 */
class RadioButtons extends SortWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    parent::exposedFormAlter($form, $formState);

    foreach ($this->sortElements as $element) {
      if (!empty($form[$element])) {
        $form[$element]['#theme'] = 'trufil_radios';
        $form[$element]['#type'] = 'radios';
      }
    }
  }

}
