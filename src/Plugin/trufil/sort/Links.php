<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\sort;

use Drupal\Core\Form\FormStateInterface;

/**
 * Radio Buttons sort widget implementation.
 *
 * @TrufilSortWidget(
 *   id = "trufil_links",
 *   label = @Translation("Links"),
 * )
 */
class Links extends SortWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    $view = $formState->get('view');
    parent::exposedFormAlter($form, $formState);

    foreach ($this->sortElements as $element) {
      if (!empty($form[$element])) {
        $form[$element]['#theme'] = 'trufil_links';

        // Exposed form displayed as blocks can appear on pages other than
        // the view results appear on. This can cause problems with
        // select_as_links options as they will use the wrong path. We
        // provide a hint for theme functions to correct this.
        $form[$element]['#trufil_path'] = $this->getExposedFormActionUrl($formState);
        if ($view->ajaxEnabled() || $view->display_handler->ajaxEnabled()) {
          $form[$element]['#attributes']['class'][] = 'trufil-links-use-ajax';
          $form['#attached']['library'][] = 'trufil/links_use_ajax';
        }
      }
    }
  }

}
