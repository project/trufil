<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\sort;

use Drupal\Core\Form\FormStateInterface;

/**
 * Default widget implementation.
 *
 * @TrufilSortWidget(
 *   id = "default",
 *   label = @Translation("Default"),
 * )
 */
class DefaultWidget extends SortWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    parent::exposedFormAlter($form, $formState);

    foreach ($this->sortElements as $element) {
      if (!empty($form[$element])) {
        $form[$element]['#type'] = 'select';
      }
    }
  }

}
