<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\pager;

/**
 * Default widget implementation.
 *
 * @TrufilPagerWidget(
 *   id = "default",
 *   label = @Translation("Default"),
 * )
 */
class DefaultWidget extends PagerWidgetBase {

}
