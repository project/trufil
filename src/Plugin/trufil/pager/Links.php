<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\pager;

use Drupal\Core\Form\FormStateInterface;

/**
 * Links pager widget implementation.
 *
 * @TrufilPagerWidget(
 *   id = "trufil_links",
 *   label = @Translation("Links"),
 * )
 */
class Links extends PagerWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    parent::exposedFormAlter($form, $formState);

    if (!empty($form['items_per_page'] && count($form['items_per_page']['#options']) > 1)) {
      $form['items_per_page']['#theme'] = 'trufil_links';
      $form['items_per_page']['#items_per_page'] = max($form['items_per_page']['#default_value'], key($form['items_per_page']['#options']));

      // Exposed form displayed as blocks can appear on pages other than
      // the view results appear on. This can cause problems with
      // select_as_links options as they will use the wrong path. We
      // provide a hint for theme functions to correct this.
      $form['items_per_page']['#trufil_path'] = $this->getExposedFormActionUrl($formState);
    }
  }

}
