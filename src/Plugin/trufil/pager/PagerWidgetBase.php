<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\pager;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\trufil\Plugin\TrufilWidgetBase;
use Drupal\trufil\Plugin\TrufilWidgetInterface;

/**
 * Base class for Trufil exposed pager widget plugins.
 */
abstract class PagerWidgetBase extends TrufilWidgetBase implements TrufilWidgetInterface {

  use StringTranslationTrait;

  /**
   * List of available exposed sort form element keys.
   */
  protected array $pagerElements = [
    'items_per_page',
    'offset',
  ];

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'advanced' => [
        'is_secondary' => FALSE,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable($handler = NULL, array $options = []): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState): array {
    $form = [];

    $form['advanced']['is_secondary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('This is a secondary option'),
      '#default_value' => !empty($this->configuration['advanced']['is_secondary']),
      '#states' => [
        'visible' => [
          ':input[name="exposed_form_options[trufil][general][allow_secondary]"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Places this element in the secondary options portion of the exposed form.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    $isSecondary = !empty($form['secondary']) && $this->configuration['advanced']['is_secondary'];

    foreach ($this->pagerElements as $element) {
      // Sanity check to make sure the element exists.
      if (empty($form[$element])) {
        continue;
      }

      if ($isSecondary) {
        $this->addElementToGroup($form, $formState, $element, 'secondary');
      }

      // Finally, add some metadata to the form element.
      $this->addContext($form[$element]);
    }
  }

}
