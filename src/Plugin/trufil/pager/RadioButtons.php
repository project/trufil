<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin\trufil\pager;

use Drupal\Core\Form\FormStateInterface;

/**
 * Radio Buttons pager widget implementation.
 *
 * @TrufilPagerWidget(
 *   id = "trufil_radios",
 *   label = @Translation("Radio Buttons"),
 * )
 */
class RadioButtons extends PagerWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $formState): void {
    parent::exposedFormAlter($form, $formState);

    if (!empty($form['items_per_page'])) {
      $form['items_per_page']['#type'] = 'radios';
      $form['items_per_page']['#prefix'] = '<div class="trufil-sortby trufil-select-as-radios">';
      $form['items_per_page']['#suffix'] = '</div>';
    }
  }

}
