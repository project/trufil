<?php declare(strict_types=1);

namespace Drupal\trufil\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Drupal\views\ViewExecutable;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base class for widget plugins.
 */
abstract class TrufilWidgetBase extends PluginBase implements TrufilWidgetInterface {

  use StringTranslationTrait;

  /**
   * The views executable.
   */
  protected ViewExecutable $view;

  /**
   * The views plugin this configuration will affect when exposed.
   */
  protected ViewsHandlerInterface $handler;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'plugin_id' => $this->pluginId,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setView(ViewExecutable $view): void {
    $this->view = $view;
  }

  /**
   * {@inheritDoc}
   */
  public function setViewsHandler(ViewsHandlerInterface $handler): void {
    $this->handler = $handler;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $formState): void {
    // Validation is optional.
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState): void {
    // Apply submitted form state to configuration.
    $values = $formState->getValues();
    foreach ($values as $key => $value) {
      if (array_key_exists($key, $this->configuration)) {
        // Change checkboxes array like [id1 => id1, id2 => id2, id3 => 0]
        // to [id1 => id1, id2 => id2].
        if (is_array($value) && !empty($form[$key]['#trufil_filter_checkboxes'])) {
          $value = array_keys(array_filter($value));
          $value = array_combine($value, $value);
        }

        $this->configuration[$key] = $value;
      }
      else {
        // Remove from form state.
        unset($values[$key]);
      }
    }
  }

  /*
   * Helper functions.
   */

  /**
   * Sets metadata on the form elements for easier processing.
   *
   * @param array $element
   *   The form element to apply the metadata to.
   *
   * @see https://www.drupal.org/project/drupal/issues/2511548
   */
  protected function addContext(array &$element): void {
    $element['#context'] = [
      '#plugin_type' => 'trufil',
      '#plugin_id' => $this->pluginId,
      '#view_id' => $this->view->id(),
      '#display_id' => $this->view->current_display,
    ];
  }

  /**
   * Moves an exposed form element into a field group.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Exposed views form state.
   * @param string $element
   *   The key of the form element.
   * @param string $group
   *   The name of the group element.
   */
  protected function addElementToGroup(array &$form, FormStateInterface $formState, $element, $group): void {
    // Ensure group is enabled.
    $form[$group]['#access'] = TRUE;

    // Add element to group.
    $form[$element]['#group'] = $group;

    // Persist state of collapsible field-sets with active elements.
    if (empty($form[$group]['#open'])) {
      // Use raw user input to determine if field-set should be open or closed.
      $userInput = $formState->getUserInput()[$element] ?? [0];
      // Take multiple values into account.
      if (!is_array($userInput)) {
        $userInput = [$userInput];
      }

      // Check if one or more values are set for our current element.
      $options = $form[$element]['#options'] ?? [];
      $defaultValue = $form[$element]['#default_value'] ?? key($options);
      $hasValues = array_reduce($userInput, function ($carry, $value) use ($defaultValue) {
        return $carry || ($value === $defaultValue ? '' : ($value || $defaultValue === 0));
      }, FALSE);

      if ($hasValues) {
        $form[$group]['#open'] = TRUE;
      }
    }
  }

  /**
   * Returns exposed form action URL object.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Exposed views form state.
   *
   * @return \Drupal\Core\Url
   *   Url object.
   */
  protected function getExposedFormActionUrl(FormStateInterface $formState): Url {
    $display = $formState->get('display');
    $request = \Drupal::request();

    if (isset($display['display_options']['path'])) {
      $args = [];
      if (\Drupal::routeMatch()->getRouteName() === 'views.ajax') {
        $previousUrl = $request->server->get('HTTP_REFERER');
        $urlRequest = Request::create($previousUrl);
        $urlObject = \Drupal::service('path.validator')->getUrlIfValid($urlRequest->getRequestUri());
        if ($urlObject) {
          $args = $urlObject->getRouteParameters();
        }
      }
      else {
        $route = $request->attributes->get('_route_object');
        /** @var \Symfony\Component\HttpFoundation\ParameterBag $rawParams */
        $rawParams = $request->attributes->get('_raw_variables');
        $routeParams = $request->attributes->get('_route_params');
        $map = $route->hasOption('_view_argument_map') ? $route->getOption('_view_argument_map') : [];

        foreach ($map as $attribute => $parameterName) {
          $arg = $rawParams->get($parameterName) ?? $routeParams[$parameterName];

          if (isset($arg)) {
            $args[$attribute] = $arg;
          }
        }
      }
    }

    $url = Url::createFromRequest(clone $request);
    $url->setAbsolute();

    return $url;
  }

}
