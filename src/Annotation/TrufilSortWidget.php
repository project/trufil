<?php declare(strict_types=1);

namespace Drupal\trufil\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a sort widget item annotation object.
 *
 * @see \Drupal\trufil\Plugin\TrufilFilterWidgetManager
 * @see plugin_api
 *
 * @Annotation
 */
class TrufilSortWidget extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

}
