<?php declare(strict_types=1);

namespace Drupal\trufil\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a filter widget item annotation object.
 *
 * @see \Drupal\trufil\Plugin\TrufilFilterWidgetManager
 * @see plugin_api
 *
 * @Annotation
 */
class TrufilFilterWidget extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

}
