<?php declare(strict_types=1);

namespace Drupal\trufil\Query;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Render\Element;
use Drupal\trufil\Helper\TrufilHelper;
use Symfony\Component\DependencyInjection\Container;

/**
 * Performs query processing related to exposed filters.
 *
 * After we change some filter to be the autocomplete field the view query may
 * look not as expected: try to use empty values, try to use array with
 * '=' operator, etc.
 * So here we modify the query to work as expected. All data necessary comes
 * from query tags.
 *
 * @see \Drupal\trufil\Plugin\trufil\filter\Autocomplete::addQueryTags()
 * @see \Drupal\trufil\Plugin\trufil\filter\Lists::addQueryTags()
 */
class ExposedFiltersQueryAlter {

  /**
   * Alters the query.
   *
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   The views query.
   */
  public function alter(AlterableInterface $query): void {
    // Get data from tags.
    $tags = array_keys(array_filter($query->alterTags));
    $fieldData = $this->getFieldDataFromTags($tags, $this->getFieldIdsFromTags($tags));

    // Alter conditions.
    $view = $query->alterMetaData['view'] ?? NULL;
    $this->alterConditions($query->conditions(), $view?->exposed_data ?? [], $fieldData);
  }

  /**
   * Alters query conditions recursively.
   *
   * @param \Drupal\Core\Database\Query\Condition|array $condition
   *   Query condition group or single condition.
   * @param array $exposedData
   *   View exposed form submitted data.
   * @param string[][][] $fieldData
   *   Autocomplete fields data.
   */
  private function alterConditions(&$condition, array $exposedData, array &$fieldData) {
    // Some conditions have inner conditions.
    if ($condition instanceof Condition) {
      $this->alterConditions($condition->conditions(), $exposedData, $fieldData);
    }
    elseif (is_array($condition)) {
      if (!empty($condition['field'])) {
        // Oh, there are some more inner conditions.
        if ($condition['field'] instanceof Condition) {
          $this->alterConditions($condition['field'], $exposedData, $fieldData);
        }
        // Finally we got to normal condition (field).
        elseif (is_string($condition['field'])) {
          $method = '';
          $value = NULL;
          $fieldName = $this->getFieldNameFromCondition($condition);

          // Find field data related to this condition (if any).
          if (!empty($fieldData[$fieldName])) {
            foreach ($fieldData[$fieldName] as &$item) {
              if (empty($item['#processed'])) {
                $data = &$item;
                $data['#processed'] = TRUE;
                $value = $exposedData[$data['field_id']] ?? NULL;
                $method = 'alterCondition' . (Container::camelize($data['type'] ?? ''));
                break;
              }
            }
          }

          // Alter the condition.
          if ($method && method_exists($this, $method)) {
            $this->{$method}($condition, $value, $data);
          }
        }
      }
      // Oh, there are some more inner conditions.
      else {
        foreach (Element::children($condition) as $i) {
          $this->alterConditions($condition[$i], $exposedData, $fieldData);

          if (empty($condition[$i])) {
            unset($condition[$i]);
          }
        }
      }
    }
  }

  /**
   * Returns field name from query condition.
   *
   * @param string[]|array $condition
   *   The views query condition.
   *
   * @return string
   *   The field name.
   */
  private function getFieldNameFromCondition(array $condition): string {
    $field = explode(' ', $condition['field']);
    $field = explode('.', $field[0]);
    return $field[1] ?? $field[0];
  }

  /**
   * Returns autocomplete fields data from query tags.
   *
   * @param string[] $tags
   *   Query tags.
   * @param string[] $fieldIds
   *   Autocomplete field ids.
   *
   * @return string[][][]
   *   The field metadata.
   */
  private function getFieldDataFromTags(array $tags, array $fieldIds): array {
    $initialData = [];
    $fieldData = [];

    foreach ($fieldIds as $fieldId) {
      $initialData[$fieldId] = ['field_id' => $fieldId];
      $prefix = "trufil__{$fieldId}__";

      foreach ($tags as $tag) {
        if (str_starts_with($tag, $prefix)) {
          $item = explode('__', str_replace($prefix, '', $tag), 2);
          $initialData[$fieldId][$item[0]] = $item[1];
        }
      }
    }

    // Change the data array format by grouping the data by field name
    // (to make it possible to process multiple filters for the same field)
    foreach ($initialData as $data) {
      $fieldData[$data['field_name']][$data['field_id']] = $data;
    }

    return $fieldData;
  }

  /**
   * Returns autocomplete field names from query tags.
   *
   * @param string[] $tags
   *   Query tags.
   *
   * @return string[]
   *   Field names.
   */
  private function getFieldIdsFromTags(array $tags): array {
    $fieldTagPrefix = 'trufil__field_id__';
    $fieldNames = [];

    foreach ($tags as $tag) {
      if (str_starts_with($tag, $fieldTagPrefix)) {
        $fieldNames[] = str_replace($fieldTagPrefix, '', $tag);
      }
    }

    return $fieldNames;
  }

  /**
   * Alters query condition related to autocomplete field.
   *
   * @param array $condition
   *   The query condition.
   * @param mixed $exposedValue
   *   The exposed filter value.
   * @param array $data
   *   The filter metadata.
   */
  private function alterConditionAutocomplete(array &$condition, $exposedValue, array $data): void {
    // Fix empty filter value.
    if (is_array($exposedValue)) {
      $exposedValue = array_filter(TrufilHelper::flattenValues($exposedValue));
    }

    // Fix multiple value condition.
    if (is_array($exposedValue)) {
      if (empty($exposedValue)) {
        $condition = NULL;
      }
      else {
        $condition['value'] = $exposedValue;
        $condition['operator'] = match ($condition['operator']) {
          '=', 'LIKE', 'ILIKE' => 'IN',
          default => $condition['operator'],
        };
      }
    }

    $emptyOperators = [
      'IS NULL' => 'IS NULL',
      'IS NOT NULL' => 'IS NOT NULL',
    ];

    // Condition like `entity_id = ''` breaks the query, we don't need it.
    if (empty($condition['value']) && empty($emptyOperators[$condition['operator']]) && !empty($data)) {
      $condition = NULL;
    }
  }

  /**
   * Alters query condition related to autocomplete field.
   *
   * @param array $condition
   *   The query condition.
   * @param mixed $exposedValue
   *   The exposed filter value.
   * @param array $data
   *   The filter metadata.
   */
  private function alterConditionDate(array &$condition, $exposedValue, array $data): void {
    $this->alterConditionAutocomplete($condition, $exposedValue, $data);
  }

}
