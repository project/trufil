(function (Drupal, once) {

  /**
   * Provides a "form auto-submit" feature.
   *
   * To make a form auto submit, all you have to do is 3 things:.
   *
   * Use the "trufil/auto_submit" js library.
   *
   * On gadgets you want to auto-submit when changed, add the
   * data-trufil-auto-submit attribute. With FAPI, add:
   * @code
   *  '#attributes' => array('data-trufil-auto-submit' => ''),
   * @endcode
   *
   * If you want to have auto-submit for every form element, add the
   * data-trufil-auto-submit-full-form to the form. With FAPI, add:
   * @code
   *   '#attributes' => array('data-trufil-auto-submit-full-form' => ''),
   * @endcode
   *
   * If you want to exclude a field from the trufil-auto-submit-full-form auto
   * submission, add an attribute of data-trufil-auto-submit-exclude to the form
   * element. With FAPI, add:
   * @code
   *   '#attributes' => array('data-trufil-auto-submit-exclude' => ''),
   * @endcode
   *
   * Finally, you have to identify which button you want clicked for autosubmit.
   * The behavior of this button will be honored if it's ajax or not:
   * @code
   *  '#attributes' => array('data-trufil-auto-submit-click' => ''),
   * @endcode
   *
   * @type {{attach: Drupal.trufilAutoSubmit.attach}}
   */
  Drupal.behaviors.trufilAutoSubmit = {
    attach: function (context) {
      // When exposed as a block, the form #attributes are moved from the form
      // to the block element, thus the second selector.
      // @see \Drupal\block\BlockViewBuilder::preRender
      let selectors = 'form[data-trufil-auto-submit-full-form], [data-trufil-auto-submit-full-form] form, [data-trufil-auto-submit]';

      // The change event bubbles, so we only need to bind it to the outer form
      // in case of a full form, or a single element when specified explicitly.
      once('trufil-auto-submit', selectors, context).forEach(function ($form) {
        // Retrieve the autosubmit delay for this particular form.
        let autoSubmitDelay = $form.dataset.trufilAutoSubmitDelay || 500;

        // On change, trigger the submit immediately.
        $form.addEventListener('change', triggerSubmit);
        // On keyup, wait for a specified number of milliseconds before
        // triggering autosubmit. Each new keyup event resets the timer.
        $form.addEventListener('keyup', Drupal.debounce(triggerSubmit, autoSubmitDelay));
      });

      /**
       * Triggers form autosubmit when conditions are right.
       *
       * - Checks first that the element that was the target of the triggering
       *   event is `:text` or `textarea`, but is not `.hasDatePicker`.
       * - Checks that the keycode of the keyup was not in the list of ignored
       *   keys (navigation keys etc).
       *
       * @param {object} e - The triggering event.
       */
      function triggerSubmit(e) {
        // e.keyCode: key.
        let ignoredKeyCodes = [
          16, // Shift.
          17, // Ctrl.
          18, // Alt.
          20, // Caps lock.
          33, // Page up.
          34, // Page down.
          35, // End.
          36, // Home.
          37, // Left arrow.
          38, // Up arrow.
          39, // Right arrow.
          40, // Down arrow.
          9, // Tab.
          13, // Enter.
          27  // Esc.
        ];

        // Triggering element.
        let $target = e.target;
        let target_tag = $target.tagName.toLowerCase();
        let target_type = $target.getAttribute('type');
        let $submit = $target.closest('form').querySelector('[data-trufil-auto-submit-click]');
        let need_submit = false;

        // Don't submit on changes to excluded elements or a submit element.
        if ($target.dataset.trufilAutoSubmitExclude
          || ['submit', 'button'].includes(target_type)
        ) {
          return true;
        }

        // Submit only if this is a non-datepicker textfield and if the
        // incoming keycode is not one of the excluded values.
        if (!$target.classList.contains('hasDatepicker')
          && ['input', 'textarea'].includes(target_tag)
          && !['radio', 'checkbox'].includes(target_type)
          && !ignoredKeyCodes.includes(e.keyCode)
        ) {
          need_submit = true;
        }
        // Only trigger submit if a change was the trigger (no keyup).
        else if (e.type === 'change') {
          need_submit = true;
        }

        if (need_submit) {
          setTimeout(function () {
            $submit.click();
            // Prevent duplicate submissions.
            $submit.setAttribute('disabled', 'disabled');
          });

          setTimeout(function () {
            $submit.removeAttribute('disabled');
          }, 100);
        }
      }
    }
  };

}(Drupal, once));
