(function (once) {

  /**
   * Allows to use ajax with Trufil links.
   *
   * @type {{attach: Drupal.behaviors.trufilSelectAsLinks.attach}}
   */
  Drupal.behaviors.trufilSelectAsLinks = {
    attach: function (context, settings) {
      once('trufil-links-use-ajax', '.trufil-links.trufil-links-use-ajax', context).forEach(function ($links) {
        let links_name = $links.getAttribute('name')
        let links_multiple = $links.getAttribute('multiple');
        let $form = $links.closest('form');
        let $filters = $form.querySelectorAll('input[name^="' + links_name + '"]');

        $links.querySelectorAll('a').forEach($link => $link.addEventListener('click', function (event) {
          // Prevent following the link URL.
          event.preventDefault();

          let link_name = links_multiple ? $link.getAttribute('name') : links_name;
          let link_value = $link.getAttribute('name').substring(links_name.length).replace(/^\[|\]$/g, '');
          let $filter = $form.querySelector('input[name="' + link_name + '"]');

          if ($link.classList.contains('trufil-link--selected')) {
            // The previously selected link is selected again. Deselect it.
            $link.classList.remove('trufil-link--selected');
            $filters.forEach(filter => filter.remove());
          }
          else {
            if (!links_multiple || link_value === 'All') {
              $links.querySelectorAll('.trufil-link--selected').forEach($link => $link.classList.remove('trufil-link--selected'));
            }
            $link.classList.add('trufil-link--selected');

            if (!$filter) {
              $links.insertAdjacentHTML('afterbegin', '<input type="hidden" name="' + link_name + '" />');
              $filter = $form.querySelector('input[name="' + link_name + '"]');
            }

            $filter.value = link_value;
          }

          // Submit the form.
          $form.querySelector('.form-submit:not([data-drupal-selector*=edit-reset])').click();
        }));
      });
    }
  };

})(once);
