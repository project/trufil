# Trufil (True Exposed Filters)

The Trufil module provides useful widgets for views exposed filters.

This is a fork of a well known Better Exposed Filters module, but without jQuery
dependency. Also, native HTML 5 elements are used for some widgets now. Though
this module is similar to the original one it has its own features.

Available filter widgets:
* Date picker
* Number
* Single on/off checkbox
* Hidden
* Links (transform radios/select to links)
* List (provides option list for fields of type like 'List (text)')
* Autocomplete (as text field, radios or select)

Now it's easier to create views with user-friendly exposed filters.

If you want some more widgets to be available - feel free to contact us.
And to contribute.

## Troubleshooting

### Checkboxes error

Sometimes you may face an error like this with checkboxes filter widget:

```
TypeError: Cannot access offset of type string on string in Drupal\Component\Utility\NestedArray::setValue()
```

Keep in mind that it comes from Drupal core and the only way to fix it is
to apply a patch. Or wait until the core is modified.

### Multiple filters for the same field

Sometimes this module may work not as expected if you have several filters
in a view for the same entity field. If you have ideas on generic solution
for this - feel free to contribute.
