<?php declare(strict_types=1);

/**
 * @file
 * Hooks provided by the Trufil module.
 */

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\HandlerBase;
use Drupal\views\ViewExecutable;

/**
 * Alters trufil options before the exposed form widgets are built.
 *
 * @param array $options
 *   The Trufil options array.
 * @param \Drupal\views\ViewExecutable $view
 *   The view to which the settings apply.
 * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $displayHandler
 *   The display handler to which the settings apply.
 */
function hook_trufil_options_alter(array &$options, ViewExecutable $view, DisplayPluginBase $displayHandler) {
  // Set the min/max value of a slider.
  $settings['field_price_value']['slider_options']['trufil_slider_min'] = 500;
  $settings['field_price_value']['slider_options']['trufil_slider_max'] = 5000;
}

/**
 * Modify the array of trufil display options for an exposed filter.
 *
 * @param array $widgets
 *   The set of trufil widgets available to this filter.
 * @param \Drupal\views\Plugin\views\HandlerBase $filter
 *   The exposed views filter plugin.
 */
function hook_trufil_filter_widgets_alter(array &$widgets, HandlerBase $filter) {
  if ($filter instanceof CustomViewsFilterFoo) {
    $widgets['trufil_links'] = t('Links');
  }
}
